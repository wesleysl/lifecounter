package edu.washington.wesleysl.lifecounter;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.content.Context;
import android.widget.Toast;


public class MainActivity extends ActionBarActivity {
    private int p1Life;
    private int p2Life;
    private int p3Life;
    private int p4Life;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        p1Life = 20;
        p2Life = 20;
        p3Life = 20;
        p4Life = 20;
    }

    public void buttonp11onClick(View v) {
        p1Life++;
        Button button = (Button) v;
        TextView lifeTotal = (TextView) findViewById(R.id.textViewp1);
        lifeTotal.setText("Life: " + p1Life);
    }

    public void buttonp12onClick(View v) {
        p1Life--;
        Button button = (Button) v;
        TextView lifeTotal = (TextView) findViewById(R.id.textViewp1);
        lifeTotal.setText("Life: " + p1Life);
        checkLethal(p1Life, "Player 1");
    }

    public void buttonp13onClick(View v) {
        p1Life = p1Life + 5;
        Button button = (Button) v;
        TextView lifeTotal = (TextView) findViewById(R.id.textViewp1);
        lifeTotal.setText("Life: " + p1Life);
    }

    public void buttonp14onClick(View v) {
        p1Life = p1Life - 5;
        Button button = (Button) v;
        TextView lifeTotal = (TextView) findViewById(R.id.textViewp1);
        lifeTotal.setText("Life: " + p1Life);
        checkLethal(p1Life, "Player 1");
    }

    public void buttonp21onClick(View v) {
        p2Life++;
        Button button = (Button) v;
        TextView lifeTotal = (TextView) findViewById(R.id.textViewp2);
        lifeTotal.setText("Life: " + p2Life);
    }

    public void buttonp22onClick(View v) {
        p2Life--;
        Button button = (Button) v;
        TextView lifeTotal = (TextView) findViewById(R.id.textViewp2);
        lifeTotal.setText("Life: " + p2Life);
        checkLethal(p2Life, "Player 2");
    }

    public void buttonp23onClick(View v) {
        p2Life = p2Life + 5;
        Button button = (Button) v;
        TextView lifeTotal = (TextView) findViewById(R.id.textViewp2);
        lifeTotal.setText("Life: " + p2Life);
    }

    public void buttonp24onClick(View v) {
        p2Life = p2Life - 5;
        Button button = (Button) v;
        TextView lifeTotal = (TextView) findViewById(R.id.textViewp2);
        lifeTotal.setText("Life: " + p2Life);
        checkLethal(p2Life, "Player 2");
    }

    public void buttonp31onClick(View v) {
        p3Life++;
        Button button = (Button) v;
        TextView lifeTotal = (TextView) findViewById(R.id.textViewp3);
        lifeTotal.setText("Life: " + p3Life);
    }

    public void buttonp32onClick(View v) {
        p3Life--;
        Button button = (Button) v;
        TextView lifeTotal = (TextView) findViewById(R.id.textViewp3);
        lifeTotal.setText("Life: " + p3Life);
        checkLethal(p3Life, "Player 3");
    }

    public void buttonp33onClick(View v) {
        p3Life = p3Life + 5;
        Button button = (Button) v;
        TextView lifeTotal = (TextView) findViewById(R.id.textViewp3);
        lifeTotal.setText("Life: " + p3Life);
    }

    public void buttonp34onClick(View v) {
        p3Life = p3Life - 5;
        Button button = (Button) v;
        TextView lifeTotal = (TextView) findViewById(R.id.textViewp3);
        lifeTotal.setText("Life: " + p3Life);
        checkLethal(p3Life, "Player 3");
    }

    public void buttonp41onClick(View v) {
        p4Life++;
        Button button = (Button) v;
        TextView lifeTotal = (TextView) findViewById(R.id.textViewp4);
        lifeTotal.setText("Life: " + p4Life);
    }

    public void buttonp42onClick(View v) {
        p4Life--;
        Button button = (Button) v;
        TextView lifeTotal = (TextView) findViewById(R.id.textViewp4);
        lifeTotal.setText("Life: " + p4Life);
        checkLethal(p4Life, "Player 4");
    }

    public void buttonp43onClick(View v) {
        p4Life = p4Life + 5;
        Button button = (Button) v;
        TextView lifeTotal = (TextView) findViewById(R.id.textViewp4);
        lifeTotal.setText("Life: " + p4Life);
    }

    public void buttonp44onClick(View v) {
        p4Life = p4Life - 5;
        Button button = (Button) v;
        TextView lifeTotal = (TextView) findViewById(R.id.textViewp4);
        lifeTotal.setText("Life: " + p4Life);
        checkLethal(p4Life, "Player 4");
    }

    public void checkLethal(int curLife, String player) {
        if (curLife <= 0 ) {
            Context context = getApplicationContext();
            CharSequence text = player + " LOSES!";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
